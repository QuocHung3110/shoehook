import React from "react";

export default function ItemShoe({shoe,handleAddToCart,handleDetail}) {
  return (
    <div className="col-4 mb-4">
      <div className="card">
        <img className="card-img-top" src={shoe.image} alt="Card image cap" />
        <div className="card-body">
          <h5 className="card-title">{shoe.name}</h5>
          <p className="card-text">$ {shoe.price}</p>
          <button
            className="btn btn-outline-info mr-2"
            onClick={() => handleAddToCart(shoe)}
          >
            Add to cart <i class="bi bi-bag-heart-fill"></i>
          </button>
          <button
            className="btn btn-outline-secondary"
            onClick={() => {
              handleDetail(shoe);}}
            data-toggle="modal"
            data-target="#myModal"
          >
            Detail <i class="bi bi-bag-heart-fill"></i>
          </button>
        </div>
      </div>
    </div>
  );
}
