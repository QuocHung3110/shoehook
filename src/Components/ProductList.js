import React from "react";
import Cart from "./Cart";
import ItemShoe from "./ItemShoe";

export default function ProductList({
  products,
  cart,
  handleAddToCart,
  handleDelete,
  handleChangeQuantity,
  handleDetail,
}) {
  return (
    <div className="row">
      {/*render danh sách giày*/}
      <div className="row col-6">
        {products.map((item) => {
          return (
            <ItemShoe
              shoe={item}
              handleAddToCart={handleAddToCart}
              handleDetail={handleDetail}
            ></ItemShoe>
          );
        })}
      </div>
      {/* render danh sách giỏ hàng */}
      <div className="col-6" id="cart">
        {cart.length > 0 && (
          <Cart
            cart={cart}
            handleDelete={handleDelete}
            handleChangeQuantity={handleChangeQuantity}
          />
        )}
      </div>
    </div>
  );
}
